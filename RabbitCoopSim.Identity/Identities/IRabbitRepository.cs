﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Identity.Identities
{
    public interface IRabbitRepository
    {
        List<IFemaleRabbitRepository> _FemaleRabbits { get; set; }
        List<IMaleRabbitRepository> _MaleRabbits { get; set; }

    }
}
