﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Identity.Identities
{
    public interface IFemaleRabbitRepository
    {
        int Age { get; set; }
        int Size { get; set; }
        int LifeTime { get; set; }
        int Gender { get; set; } 

        int PregnancyTime { get; set; }      //Hamile zamanı boyunca çiftleşme gerçekleşmez
        int RestTime { get; set; }           //Doğumdan sonra dinlenme zamanı
        int BeginofFertility { get; set; }   //Doğurganlığın başladığı ay
        int EndofFertility { get; set; }     //Doğurganlığın bittiği ay
        bool isPregnant { get; set; }        //Hamile mi
        bool isFertility { get; set; }       //Doğurgan mı 
        bool isinRest { get; set; }          //Dinleniyor mu

    }
}
