﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Identity.Identities
{
    public interface IAnimalRepository
    {
        List<IRabbitRepository> _Rabbits { get; set; }
    }
}
