﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Identity.Identities
{
    public interface ICageRepository
    {
        List<IAnimalRepository> _Animals { get; set; }
        int Capacity { get; set; }
    }
}
