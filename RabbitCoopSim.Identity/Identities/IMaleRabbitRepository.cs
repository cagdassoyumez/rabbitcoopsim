﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Identity.Identities
{
    public interface IMaleRabbitRepository
    {
        int Age { get; set; }
        int Size { get; set; }
        int LifeTime { get; set; }
        int Gender { get; set; } 

        int BeginofProductivity { get; set; }
        int EndofProductivity { get; set; }
        int RestTime { get; set; }
        bool isProductive { get; set; }
        bool isMated { get; set; }
        bool isinRest { get; set; }
    }
}
