﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Identity.Identities
{
    public interface ICoopRepository
    {
        List<ICageRepository> _Cages { get; set; }
    }
}
