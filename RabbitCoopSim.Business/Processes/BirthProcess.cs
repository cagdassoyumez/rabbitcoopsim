﻿using BenchmarkDotNet.Attributes;
using RabbitCoopSim.Identity.Identities;
using RabbitCoopSim.Model.Models.Animals;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    
    public class BirthProcess
    {
        public void beginRabbitBirth(IRabbitRepository _rabbit, int maxnewbornrabbits, int percentoflifechance, int percentofnewrabbitgender, int malelifetime, int beginmaleproductivity, int endmaleproductivity, int malesize, int femalelifetime, int beginfemalefertility, int endfemalefertility, int femalesize)
        {

            FemaleRabbitProcess frp = new FemaleRabbitProcess();
            MaleRabbitProcess mrp = new MaleRabbitProcess();
            Random rnd = new Random();
            int randomresult = 0;
            for(int i = 0; i< maxnewbornrabbits; i++) {
                randomresult = rnd.Next(1, 100);
                if (randomresult < percentoflifechance) {

                    randomresult = rnd.Next(1, 100);
                    if(randomresult < percentofnewrabbitgender)
                    {
                        IMaleRabbitRepository _maleRabbit = new MaleRabbit();

                        mrp.getNewMaleRabbit(_maleRabbit, malelifetime, beginmaleproductivity, endmaleproductivity, malesize);
                        _rabbit._MaleRabbits.Add(_maleRabbit);
                    }
                    else
                    {
                        IFemaleRabbitRepository _femaleRabbit = new FemaleRabbit();

                        frp.getNewFemaleRabbit(_femaleRabbit, femalelifetime, beginfemalefertility, endfemalefertility, femalesize);
                        _rabbit._FemaleRabbits.Add(_femaleRabbit);
                    }
                }
            }


        }
    }
}
