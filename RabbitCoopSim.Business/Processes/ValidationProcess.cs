﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    public class ValidationProcess
    {
        public bool UserInputValidation(string userInput)
        {
            int month;
            bool parseSuccess = int.TryParse(userInput, out month);

            if (parseSuccess) { 
                return true;
            }
            else { 
                Console.WriteLine("This is not a number!");
                return false;
            }
        }

    }
}
