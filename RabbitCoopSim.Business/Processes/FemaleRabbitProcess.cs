﻿using RabbitCoopSim.Identity.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    public class FemaleRabbitProcess
    {
        public void getNewFemaleRabbit(IFemaleRabbitRepository _femaleRabbit, int Lifetimeoffemalerabbits, int Thebegintimeoffemalerabbitfertility, int Theendtimeoffemalerabbitfertility, int Femalerabbitsize)
        {
            _femaleRabbit.Age = 0;
            _femaleRabbit.Size = Femalerabbitsize;
            _femaleRabbit.LifeTime = Lifetimeoffemalerabbits;
            _femaleRabbit.BeginofFertility = Thebegintimeoffemalerabbitfertility;
            _femaleRabbit.EndofFertility = Theendtimeoffemalerabbitfertility;
            _femaleRabbit.RestTime = 0;
            _femaleRabbit.PregnancyTime = 0;
            _femaleRabbit.isFertility = false;
            _femaleRabbit.isPregnant = false;
            _femaleRabbit.isinRest = false;
        }
        
    }
}
