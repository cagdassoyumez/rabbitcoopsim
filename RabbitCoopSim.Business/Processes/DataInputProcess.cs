﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    public class DataInputProcess
    {
        public Dictionary<string, string> getInputDataFromTextFile()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            try
            {
                using (StreamReader reader = new StreamReader("C:\\Users\\csoyumez\\Source\\Repos\\RabbitCoopSim\\RabbitCoopSim\\Data.txt"))
                {
                    string fulltext = "";
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        fulltext += line + "|";
                        Console.WriteLine(line);
                    }
                    dict = fulltext.Trim('|').Split('|')
                        .Select(x => x.Split(':'))
                        .ToDictionary(x => x[0], x => x[1]);
                }
                return dict;
            }
            catch (Exception e)
            {
                Console.WriteLine("Hatayı kontrol ediniz.:");
                Console.WriteLine(e.Message);
                return dict;
            }
        }
    }
}
