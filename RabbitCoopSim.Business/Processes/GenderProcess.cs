﻿using RabbitCoopSim.Model.Models.Animals;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    public class GenderProcess
    {
        public string GetGender(int gender)
        {
            // The switch here is less readable because of these integral numbers
            switch (gender)
            {
                case 0:
                    return "Male";
                case 1:
                    return "Female";
                default:
                    return "Invalid Data for Gender";
            }
        }
    }
}
