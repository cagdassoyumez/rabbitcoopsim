﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    [RankColumn]
    [MemoryDiagnoser]
    public class FillDataProcess
    {
        [Benchmark]
        public int fillTheBeginData(Dictionary<string, string> inputdatadict, string key)
        {
            ValidationProcess vp = new ValidationProcess();
            string value;
            int result;
            if (inputdatadict.TryGetValue(key, out value))
            {
                if (vp.UserInputValidation(value))
                {
                    if (int.TryParse(value, out result))
                    {
                        return result;
                    }
                }
            }
            return 0;
        }
    }
}
