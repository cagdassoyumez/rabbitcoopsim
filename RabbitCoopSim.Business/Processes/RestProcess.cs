﻿using RabbitCoopSim.Identity.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{

    public class RestProcess
    {
        public int decreaseRestTime(bool isinRest, int restTime)
        {
            if (isinRest == true && restTime > 0)
            {
                restTime = restTime - 1;
                return restTime--;
            }
            else
            {
                return restTime;
            }
        }

        public void beginFemaleAfterBirthRest(IFemaleRabbitRepository _femaleRabbit, int resttimeafterbirth)
        {
            _femaleRabbit.RestTime = resttimeafterbirth;
            _femaleRabbit.isFertility = true;
            _femaleRabbit.isPregnant = false;
            _femaleRabbit.isinRest = true;
        }

        public void endFemaleAfterBirthRest(IFemaleRabbitRepository _femaleRabbit)
        {
            _femaleRabbit.isFertility = true;
            _femaleRabbit.isPregnant = false;
            _femaleRabbit.isinRest = false;
        }

        public void beginFemaleOldAgeFullRest(IFemaleRabbitRepository _femalerabbit, int lifeTime)
        {
            _femalerabbit.isinRest = true;
            _femalerabbit.RestTime = lifeTime;
        }

        public void beginMaleAfterMatedRest(IMaleRabbitRepository _maleRabbit, int resttimeaftermating)
        {
            _maleRabbit.RestTime = resttimeaftermating;
            _maleRabbit.isMated = false;
            _maleRabbit.isinRest = true;
        }

        public void endMaleAfterMatedRest(IMaleRabbitRepository maleRabbit)
        {
            maleRabbit.isProductive = true;
            maleRabbit.isMated = false;
            maleRabbit.isinRest = false;
        }

        public void beginMaleOldAgeFullRest(IMaleRabbitRepository _malerabbit, int lifeTime)
        {
            _malerabbit.isinRest = true;
            _malerabbit.RestTime = lifeTime;
        }
    }
}
