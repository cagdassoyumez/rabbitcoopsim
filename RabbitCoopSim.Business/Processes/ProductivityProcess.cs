﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    [RankColumn]
    [MemoryDiagnoser]
    public class ProductivityProcess
    {
        [Arguments(2, 2, 9, true)]
        [Arguments(2, 2, 9, false)]
        [Arguments(9, 2, 9, true)]
        [Arguments(9, 2, 9, false)]
        [Arguments(0, 2, 9, true)]
        [Arguments(0, 2, 9, false)]
        [Benchmark]
        public bool changeProductivityWithAge(int age, int productivitybegintime, int productivityendtime, bool isproductive)
        {
            if (age == productivitybegintime)
                return true;
            else if (age == productivityendtime)
                return false;
            else
                return isproductive;
        }
    }
}
