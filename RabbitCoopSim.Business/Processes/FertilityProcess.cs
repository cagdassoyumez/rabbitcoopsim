﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    [RankColumn]
    [MemoryDiagnoser]
    public class FertilityProcess
    {
        [Arguments(2, 2, 9, true)]
        [Arguments(2, 2, 9, false)]
        [Arguments(9, 2, 9, true)]
        [Arguments(9, 2, 9, false)]
        [Benchmark]
        public bool changeFertilityWithAge(int age, int fertilitybegintime, int fertilityendtime, bool isfertility)
        {
            if (age == fertilitybegintime)
                return true;
            else if (age == fertilityendtime)
                return false;
            else
                return isfertility;
        }
    }
}
