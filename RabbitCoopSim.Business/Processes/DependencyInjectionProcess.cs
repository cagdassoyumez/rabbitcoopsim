﻿using Microsoft.Extensions.DependencyInjection;
using RabbitCoopSim.Identity.Identities;
using RabbitCoopSim.Model.Models.Animals;
using RabbitCoopSim.Model.Models.Facilities;
using RabbitCoopSim.Model.Models.Shelters;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    public class DependencyInjectionProcess
    {
        public static ServiceProvider Configuration()
        {
            ServiceProvider serviceProvider = new ServiceCollection()

                                .AddSingleton<IAnimalRepository, Animal>()
                                .AddSingleton<IRabbitRepository, Rabbit>()
                                .AddSingleton<IFemaleRabbitRepository, FemaleRabbit>()
                                .AddSingleton<IMaleRabbitRepository, MaleRabbit>()
                                .AddSingleton<ICageRepository, Cage>()
                                .AddSingleton<ICoopRepository, Coop>()

                                .BuildServiceProvider();

            return serviceProvider;
        }
    }
}
