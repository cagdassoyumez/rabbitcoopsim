﻿using BenchmarkDotNet.Attributes;
using RabbitCoopSim.Identity.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    public class MatingProcess
    {
        public void startRabbitMating(IFemaleRabbitRepository _femalerabbit, IMaleRabbitRepository _malerabbit, int MaleRestTime, int FemalePregnancyTime)
        {
            _malerabbit.isinRest = true;
            _malerabbit.isMated = true;
            _malerabbit.RestTime = MaleRestTime;

            _femalerabbit.isPregnant = true;
            _femalerabbit.isFertility = false;
            _femalerabbit.PregnancyTime = FemalePregnancyTime;
        }
    }
}
