﻿using RabbitCoopSim.Identity.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Business.Processes
{
    public class MaleRabbitProcess
    {
        public void getNewMaleRabbit(IMaleRabbitRepository _maleRabbit, int Lifetimeofmalerabbits, int Thebegintimeofmalerabbitproductivity, int Theendtimeofmalerabbitproductivity, int Malerabbitsize)
        {
            _maleRabbit.Age = 0;
            _maleRabbit.Size = Malerabbitsize;
            _maleRabbit.LifeTime = Lifetimeofmalerabbits;
            _maleRabbit.BeginofProductivity = Thebegintimeofmalerabbitproductivity;
            _maleRabbit.EndofProductivity = Theendtimeofmalerabbitproductivity;
            _maleRabbit.RestTime = 0;
            _maleRabbit.isProductive = false;
            _maleRabbit.isMated = false;
            _maleRabbit.isinRest = false;
        }
    }
}
