﻿using RabbitCoopSim.Business.Processes;
using RabbitCoopSim.Identity.Identities;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using BenchmarkDotNet.Running;
using System.IO;
using RabbitCoopSim.Model.Models.Animals;
using System.Threading.Tasks;
using System.Linq;

namespace RabbitCoopSim
{
    public class Program
    {
        
        public static void Main(string[] args)
        {
            int Lifetimeofmalerabbits = 0;
            int Lifetimeoffemalerabbits = 0;
            int Thenumberofnewbornrabbits = 0;
            int Percentageoflifechancenewbornrabbit = 0;
            int Percentageofnewbornrabbitgenderismale = 0;
            int Thepregnancytimeoffemalerabbit = 0;
            int Theresttimeofmalerabbitaftermating = 0;
            int Theresttimeoffemalerabbitafterbirth = 0;
            int Thebegintimeofmalerabbitproductivity = 0;
            int Theendtimeofmalerabbitproductivity = 0;
            int Thebegintimeoffemalerabbitfertility = 0;
            int Theendtimeoffemalerabbitfertility = 0;
            int Femalerabbitsize = 0;
            int Malerabbitsize = 0;
            int Cagecapacity = 0;

            var serviceProvider = DependencyInjectionProcess.Configuration();
            BenchmarkProcess bp = new BenchmarkProcess();
            bp.ConcatStringsUsingGenericList();
            bp.ConcatStringsUsingStringBuilder();
            DataInputProcess dip = new DataInputProcess();
            Dictionary<string, string> inputDataDict = dip.getInputDataFromTextFile();

            

            var _animal = serviceProvider.GetService<IAnimalRepository>();
            var _rabbit = serviceProvider.GetService<IRabbitRepository>();
            var _coop = serviceProvider.GetService<ICoopRepository>();
            var _cage = serviceProvider.GetService<ICageRepository>();
            var _rabbitAdam = serviceProvider.GetService<IMaleRabbitRepository>();
            var _rabbitEve = serviceProvider.GetService<IFemaleRabbitRepository>();
            
            BenchmarkDotNet.Reports.Summary summary;
            
            //text dosyasından dictionarye aktardığımız dataları daha sonra kullanmak için parametrelere basıyoruz. 
            FillDataProcess fdp = new FillDataProcess();
            Lifetimeofmalerabbits = fdp.fillTheBeginData(inputDataDict, "Lifetimeofmalerabbits");
            Lifetimeoffemalerabbits = fdp.fillTheBeginData(inputDataDict, "Lifetimeoffemalerabbits");
            Thenumberofnewbornrabbits = fdp.fillTheBeginData(inputDataDict, "Thenumberofnewbornrabbits");

            Percentageoflifechancenewbornrabbit = fdp.fillTheBeginData(inputDataDict, "Percentageoflifechancenewbornrabbit");
            Percentageofnewbornrabbitgenderismale = fdp.fillTheBeginData(inputDataDict, "Percentageofnewbornrabbitgenderismale");
            Thepregnancytimeoffemalerabbit = fdp.fillTheBeginData(inputDataDict, "Thepregnancytimeoffemalerabbit");

            Theresttimeofmalerabbitaftermating = fdp.fillTheBeginData(inputDataDict, "Theresttimeofmalerabbitaftermating");
            Theresttimeoffemalerabbitafterbirth = fdp.fillTheBeginData(inputDataDict, "Theresttimeoffemalerabbitafterbirth");

            Thebegintimeofmalerabbitproductivity = fdp.fillTheBeginData(inputDataDict, "Thebegintimeofmalerabbitproductivity");
            Theendtimeofmalerabbitproductivity = fdp.fillTheBeginData(inputDataDict, "Theendtimeofmalerabbitproductivity");

            Thebegintimeoffemalerabbitfertility = fdp.fillTheBeginData(inputDataDict, "Thebegintimeoffemalerabbitfertility");
            Theendtimeoffemalerabbitfertility = fdp.fillTheBeginData(inputDataDict, "Theendtimeoffemalerabbitfertility");

            Femalerabbitsize = fdp.fillTheBeginData(inputDataDict, "Femalerabbitsize");
            Malerabbitsize = fdp.fillTheBeginData(inputDataDict, "Malerabbitsize");
            Cagecapacity = fdp.fillTheBeginData(inputDataDict, "Cagecapacity");

            int time = 0;

            GenderProcess gp = new GenderProcess();
            string gender = gp.GetGender(_rabbitAdam.Gender);

            _coop._Cages = new List<ICageRepository>();
            _cage._Animals = new List<IAnimalRepository>();
            _animal._Rabbits = new List<IRabbitRepository>();
            _rabbit._MaleRabbits = new List<IMaleRabbitRepository>();
            _rabbit._FemaleRabbits = new List<IFemaleRabbitRepository>();

            MaleRabbitProcess mrp = new MaleRabbitProcess();
            FemaleRabbitProcess frp = new FemaleRabbitProcess();
            mrp.getNewMaleRabbit(_rabbitAdam, Lifetimeofmalerabbits, Thebegintimeofmalerabbitproductivity, Theendtimeofmalerabbitproductivity, Malerabbitsize );         
            frp.getNewFemaleRabbit(_rabbitEve, Lifetimeoffemalerabbits, Thebegintimeoffemalerabbitfertility, Theendtimeoffemalerabbitfertility, Femalerabbitsize );
            _rabbit._MaleRabbits.Add(_rabbitAdam);
            _rabbit._FemaleRabbits.Add(_rabbitEve);
            _animal._Rabbits.Add(_rabbit);
            
            _cage.Capacity = Cagecapacity;
            _cage._Animals.Add(_animal);

            _coop._Cages.Add(_cage);

            bool flag = true;
            while (flag)
            {
                Console.WriteLine("Enter Coop Simulation Month: ");
                string monthInput = Console.ReadLine();
                ValidationProcess vp = new ValidationProcess();
                while (!vp.UserInputValidation(monthInput))
                {
                    Console.WriteLine("Input is not a number. Please Enter Coop Simulation Month: ", monthInput);
                    monthInput = Console.ReadLine();
                }
                int month = int.Parse(monthInput);
                Console.WriteLine("Month input is {0}. At the beginning there are 2 rabbits in coop. male: 1, female: 1", month);


                ProductivityProcess pp = new ProductivityProcess();
                RestProcess rp = new RestProcess();
                for (time = 0; time < month; time++)
                {

                    Parallel.ForEach(_rabbit._MaleRabbits, _malerabbit =>
                    {
                        //yaşa göre üretkenlik
                        _malerabbit.isProductive = pp.changeProductivityWithAge(_malerabbit.Age, Thebegintimeofmalerabbitproductivity, Theendtimeofmalerabbitproductivity, _malerabbit.isProductive);   //yaşa göre ayı gelen erkek tavşanların üretkenliği değiştirme

                        //eğer dinleniyorsa dinlenme zamanını azaltıyoruz
                        _malerabbit.RestTime = rp.decreaseRestTime(_malerabbit.isinRest,_malerabbit.RestTime);
                        
                        //çiftleşme sonrası dinlenme başlangıcı
                        if (_malerabbit.isProductive == true && _malerabbit.isMated == true && _malerabbit.isinRest == false)
                        {
                            rp.beginMaleAfterMatedRest(_malerabbit, Theresttimeofmalerabbitaftermating);
                        }

                        //çiftleşme sonrası dinlenme bitiş
                        if (_malerabbit.RestTime == 0 && _malerabbit.isProductive == true && _malerabbit.isMated == false && _malerabbit.isinRest == true)
                        {
                            rp.endMaleAfterMatedRest(_malerabbit);
                        }

                        //üretkenlik bitince dinlenmeye geçme
                        if (_malerabbit.Age == Theendtimeofmalerabbitproductivity) {  
                            rp.beginMaleOldAgeFullRest(_malerabbit, Lifetimeofmalerabbits);
                        }

                        //tavşanın yaşını arttırıyoruz
                        _malerabbit.Age++;
                    });


                    FertilityProcess fp = new FertilityProcess();
                    BirthProcess brp = new BirthProcess();
                    Parallel.ForEach(_rabbit._FemaleRabbits, _femalerabbit =>
                    {
                        //yaşa göre doğurganlık
                        _femalerabbit.isFertility = fp.changeFertilityWithAge(_femalerabbit.Age, Thebegintimeoffemalerabbitfertility, Theendtimeoffemalerabbitfertility, _femalerabbit.isFertility);   //yaşa göre ayı gelen dişi tavşanların doğurganlığı değiştirme
                        
                        //eğer dinleniyorsa dinlenme zamanını azaltıyoruz
                        _femalerabbit.RestTime = rp.decreaseRestTime(_femalerabbit.isinRest, _femalerabbit.RestTime);


                        //doğum işlemleri hamilelik varsa ve doğurganlık yoksa
                        if (_femalerabbit.PregnancyTime == 0 && _femalerabbit.isPregnant == true && _femalerabbit.isFertility == false)
                        {
                            brp.beginRabbitBirth(_rabbit, Thenumberofnewbornrabbits, Percentageoflifechancenewbornrabbit, Percentageofnewbornrabbitgenderismale, Lifetimeofmalerabbits, Thebegintimeofmalerabbitproductivity, Theendtimeofmalerabbitproductivity, Malerabbitsize, Lifetimeoffemalerabbits, Thebegintimeoffemalerabbitfertility, Theendtimeoffemalerabbitfertility, Femalerabbitsize);
                        }

                        //doğum sonrası dinlenme başlangıcı
                        if (_femalerabbit.PregnancyTime == 0 && _femalerabbit.isFertility == false && _femalerabbit.isPregnant == true && _femalerabbit.isinRest == false)
                        {
                            rp.beginFemaleAfterBirthRest(_femalerabbit, Theresttimeoffemalerabbitafterbirth);
                        }   
                        
                        //doğum sonrası dinlenme bitiş
                        if (_femalerabbit.RestTime == 0 && _femalerabbit.isFertility == true && _femalerabbit.isPregnant == false && _femalerabbit.isinRest == true)
                        {
                            rp.endFemaleAfterBirthRest(_femalerabbit);
                        }

                        //doğurganlık bitince dinlenmeye geçme
                        if (_femalerabbit.Age == Theendtimeoffemalerabbitfertility)
                        {
                            rp.beginFemaleOldAgeFullRest(_femalerabbit, Lifetimeoffemalerabbits);
                        }

                        //eğer hamileyse hamilelik zamanını azaltıyoruz
                        if (_femalerabbit.isPregnant == true && _femalerabbit.PregnancyTime > 0)
                        {
                            _femalerabbit.PregnancyTime--;
                        }
                        //tavşanın yaşını arttırıyoruz
                        _femalerabbit.Age++;
                        
                    });

                    

                    //çiftleşmeye uygun tavşanları seçeceğiz
                    Parallel.ForEach( _rabbit._MaleRabbits.Where(x => x.isProductive == true && x.isMated == false && x.isinRest == false), (_maleRabbit,state1) =>
                    {
                        Parallel.ForEach(_rabbit._FemaleRabbits.Where(n => n.isFertility == true && n.isPregnant == false && n.isinRest == false), (_femaleRabbit, state) =>
                        {
                            MatingProcess mp = new MatingProcess();
                            mp.startRabbitMating(_femaleRabbit, _maleRabbit, Theresttimeofmalerabbitaftermating, Thepregnancytimeoffemalerabbit);

                            if(_femaleRabbit.isPregnant == true && _maleRabbit.isMated == true)
                            {
                                state.Break();
                            }
                        });
                    });
                    
                    
                    //yaşı lifetime dan büyük olanları siliyoruz
                    _rabbit._FemaleRabbits.RemoveAll(x => x.Age > x.LifeTime);
                    _rabbit._MaleRabbits.RemoveAll(x => x.Age > x.LifeTime);

                }

                
                Console.WriteLine("At the end there are {0} rabbits in coop. female: {1}, male: {2}", _rabbit._FemaleRabbits.Count + _rabbit._MaleRabbits.Count, _rabbit._FemaleRabbits.Count, _rabbit._MaleRabbits.Count);
                Console.WriteLine("number of pregnant female : {0} " , _rabbit._FemaleRabbits.Where(x => x.isPregnant == true ).Count());
                Console.WriteLine("number of newborn male : {0} \number of newborn female : {1}  ", _rabbit._MaleRabbits.Count(x => x.Age == 0), _rabbit._FemaleRabbits.Count(x => x.Age == 0));
                Console.WriteLine("number of resting female after end of fertility: {0} \number of resting male after end of productivity: {1}  ", _rabbit._FemaleRabbits.Count(x => x.Age >= Theendtimeoffemalerabbitfertility && x.isFertility == false), _rabbit._MaleRabbits.Count(x => x.Age >= Theendtimeofmalerabbitproductivity && x.isProductive == false));
                Console.WriteLine("number of resting female after birth: {0} \number of resting male after mating: {1}  ", _rabbit._FemaleRabbits.Count(x => x.isFertility == true && x.isinRest == true), _rabbit._MaleRabbits.Count( x => x.isProductive == true && x.isinRest == true));

                
                summary = BenchmarkRunner.Run<FertilityProcess>();
                summary = BenchmarkRunner.Run<ProductivityProcess>();
            }
        }
    }
}
