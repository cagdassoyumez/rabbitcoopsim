``` ini

BenchmarkDotNet=v0.12.1, OS=Windows 10.0.17134.471 (1803/April2018Update/Redstone4)
Intel Core i7-3770 CPU 3.40GHz (Ivy Bridge), 1 CPU, 8 logical and 4 physical cores
Frequency=3312789 Hz, Resolution=301.8605 ns, Timer=TSC
.NET Core SDK=2.2.402
  [Host]     : .NET Core 2.2.7 (CoreCLR 4.6.28008.02, CoreFX 4.6.28008.03), X64 RyuJIT
  DefaultJob : .NET Core 2.2.7 (CoreCLR 4.6.28008.02, CoreFX 4.6.28008.03), X64 RyuJIT


```
|                 Method | age | fertilitybegintime | fertilityendtime | isfertility |      Mean |     Error |    StdDev |    Median | Rank | Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------------- |---- |------------------- |----------------- |------------ |----------:|----------:|----------:|----------:|-----:|------:|------:|------:|----------:|
| **changeFertilityWithAge** |   **2** |                  **2** |                **9** |       **False** | **0.0142 ns** | **0.0229 ns** | **0.0203 ns** | **0.0023 ns** |    **1** |     **-** |     **-** |     **-** |         **-** |
| **changeFertilityWithAge** |   **2** |                  **2** |                **9** |        **True** | **0.0002 ns** | **0.0007 ns** | **0.0007 ns** | **0.0000 ns** |    **1** |     **-** |     **-** |     **-** |         **-** |
| **changeFertilityWithAge** |   **9** |                  **2** |                **9** |       **False** | **0.5516 ns** | **0.0435 ns** | **0.0407 ns** | **0.5543 ns** |    **3** |     **-** |     **-** |     **-** |         **-** |
| **changeFertilityWithAge** |   **9** |                  **2** |                **9** |        **True** | **0.5152 ns** | **0.0481 ns** | **0.0554 ns** | **0.4946 ns** |    **2** |     **-** |     **-** |     **-** |         **-** |
