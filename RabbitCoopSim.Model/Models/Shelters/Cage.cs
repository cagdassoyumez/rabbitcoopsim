﻿using RabbitCoopSim.Identity.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Model.Models.Shelters
{
    public class Cage : ICageRepository
    {
        public List<IAnimalRepository> _Animals { get; set; }
        public int Capacity { get; set; }

      }
}
