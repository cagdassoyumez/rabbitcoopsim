﻿using RabbitCoopSim.Identity.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Model.Models.Animals
{
    public class FemaleRabbit : IFemaleRabbitRepository
    {
        public int Age { get; set; }
        public int Size { get; set; }
        public int LifeTime { get; set; }
        public int Gender { get; set; } = 1;

        public int PregnancyTime { get; set; }
        public int RestTime { get; set; }
        public int BeginofFertility { get; set; }
        public int EndofFertility { get; set; }
        public bool isPregnant { get; set; }
        public bool isFertility { get; set; }
        public bool isinRest { get; set; }
        
    }
}
