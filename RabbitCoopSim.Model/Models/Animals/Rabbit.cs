﻿using RabbitCoopSim.Identity.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Model.Models.Animals
{
    public class Rabbit : IRabbitRepository
    {
        public List<IFemaleRabbitRepository> _FemaleRabbits { get; set; }
        public List<IMaleRabbitRepository> _MaleRabbits { get; set; }

    }
}
