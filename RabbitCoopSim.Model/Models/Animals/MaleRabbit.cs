﻿using RabbitCoopSim.Identity.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitCoopSim.Model.Models.Animals
{
    public class MaleRabbit : IMaleRabbitRepository
    {

        public int Age { get; set; }
        public int Size { get; set; }
        public int LifeTime { get; set; }
        public int Gender { get; set; } = 0;
        
        public int RestTime { get; set; }
        public int BeginofProductivity { get; set; }
        public int EndofProductivity { get; set; }
        public bool isProductive { get; set; }
        public bool isMated { get; set; }
        public bool isinRest { get; set; }
    }
}
